# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=httm
pkgver=0.37.8
pkgrel=0
pkgdesc="Interactive, file-level Time Machine-like tool for ZFS/btrfs"
url="https://github.com/kimono-koans/httm"
# riscv64: TODO
# s390x: fails to build nix crate
arch="all !riscv64 !s390x"
license="MPL-2.0"
makedepends="cargo acl-dev cargo-auditable"
subpackages="$pkgname-doc"
source="https://github.com/kimono-koans/httm/archive/refs/tags/$pkgver/httm-$pkgver.tar.gz"
options="net !check"  # no tests provided

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release --features acls
}

package() {
	install -D -m755 target/release/$pkgname -t "$pkgdir"/usr/bin/
	install -D -m644 $pkgname.1 -t "$pkgdir"/usr/share/man/man1/
}

sha512sums="
239bff56155051161ac659fd1a2b15d0e65775462a2571e910170cb7f7b9159066c843386cae36822e7047e4c39cd06abc93fd6389c426f68a6f02bb355ac15f  httm-0.37.8.tar.gz
"
